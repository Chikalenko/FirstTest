package Animals;

public class Dog extends Animal {
    private int countLeg;

    public int getCountLeg() {
        return countLeg;
    }

    public void setCountLeg(int countLeg) {
        this.countLeg = countLeg;
    }

    @Override
    public void Say() {
        System.out.println("ГАВ!");
    }
}
