import Animals.Dog;
import Animals.Animal;
import Animals.Cat;

public class Main3 {
    public static void main(String[] a){
        Dog dog = new Dog();
        dog.setName("Жучка");
        dog.setAge(12);
        dog.setCountLeg(4);
        dog.HappyBirthday();
        System.out.println(dog.getAge());
        dog.Say();

        Cat cat = new Cat();
        cat.setName("Барсик");
        cat.setAge(1);
        System.out.println(cat.getAge());
        cat.Say();
    }
}
